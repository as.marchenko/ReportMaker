package script;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class FTPDriver {
    public String host;
    public String user;
    public String pass;
    public int port;
    private Utils utils = new Utils();

    final org.apache.commons.net.ftp.FTPClient ftpClient = new org.apache.commons.net.ftp.FTPClient();

    public void Connect(final String host, final String user, final String pass, final int port) throws Exception {
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.port = port;
        try {
            ftpClient.setConnectTimeout(3000);
            ftpClient.connect(host,port);
        } catch (Exception e){
            System.out.println("[ERROR] Cant connect to FTP server: " + e.getMessage());
        }
        ftpClient.user(user);
        ftpClient.pass(pass);
        ftpClient.setDataTimeout(20000);

        if (ftpClient.getReplyCode()==230){
            ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
        } else {
            System.out.println("[ERROR] FTP server refused connection: "+ftpClient.getReplyString());
            ftpClient.disconnect();
            System.exit(1);
        }
    }
    public void Close() throws Exception {
        this.ftpClient.disconnect();
    }
    public List<String> fileList(final String path) throws Exception{
        FTPFile[] files = this.ftpClient.listFiles(path);
        List<String> result = new ArrayList<String>();
        for (FTPFile file : files){
            if (file.isFile()) result.add(file.getName());
        }
        return result;
    }
    public byte[] getFile(final String filename) throws Exception {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        try {
            this.ftpClient.retrieveFile(filename,byteArrayOutputStream);
        } catch (Exception e) {
            throw new Exception("Can't get file "+filename);
        } finally {
            byteArrayOutputStream.close();
        }
        return byteArrayOutputStream.toByteArray();
    }
    public boolean putFile(final String filename, final String folder, final byte[] body) throws Exception {
        boolean result = false;
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(body);
        try {
            result = this.ftpClient.storeFile(folder + filename, byteArrayInputStream);
            if (result) utils.log("File "+filename+" was uploaded to "+folder);
            else utils.log("[ERROR] Can't upload "+filename+" to "+folder);
        } finally {
            byteArrayInputStream.close();
        }
        return result;
    }
    public void deleteFile(final String pathname) throws Exception {
        final boolean result = this.ftpClient.deleteFile(pathname);
        if (!result) utils.log("[ERROR] Can't delete file "+pathname);
    }
}