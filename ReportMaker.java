package script;

import java.util.*;
import org.apache.commons.codec.digest.DigestUtils;

public class ReportMaker {

    private static final FTPDriver ftp = new FTPDriver();
    private static final String host = "ftpex.edi.su";
    private static final String login = "ficus";
    private static final String pass = "ZCegthFlvbyeXite";
    private static final int port = 21;

    private static final String directoryFrom = "cinbox/";
    private static final String directoryTo = "transit/";

    private List<String> files = new ArrayList<String>();
    public Set<String> prefixes = new HashSet<String>();
    private StringBuffer stringBuffer = new StringBuffer();
    private Utils utils = new Utils();

    public ReportMaker() throws Exception {
        utils.log("Start script");
        ftp.Connect(host,login,pass,port);
        files = ftp.fileList(directoryFrom);
        if (!files.isEmpty()){
            Collections.sort(files);
            for(String name : files){
                String[] prefs = name.split("_");
                prefixes.add(prefs[0]);
            }
            Iterator<String> iterator = prefixes.iterator();
            while (iterator.hasNext()){
                ReportCreate(iterator.next());
            }
        } else {
            utils.log("No files in directory "+directoryFrom);
        }
        utils.log("Script end");
        files.clear();
        ftp.Close();
    }
    public void ReportCreate(final String mask) throws Exception {
        createBuffer();
        for (String file : files){
            if (file.contains(mask)){
                utils.log("processing file "+file);
                final byte[] body = ftp.getFile(directoryFrom+file);
                final String filebody = new String(body);
                final String[] strings = filebody.split("\n");
                for (int s=1; s<strings.length; s++){
                    final String[] substring = strings[s].split(";");
                    final String key = substring[3]+";"+substring[4];
                    final int quantity = Integer.parseInt(substring[9]);
                    if(!stringBuffer.toString().contains(key)){
                        addString(strings[s]);
                    } else {
                        modifiedString(key,quantity);
                    }
                }
                ftp.deleteFile(directoryFrom+file);
            }
        }
        String uniqpart = DigestUtils.md5Hex(mask).substring(0,10);
        StringBuilder bildFilename = new StringBuilder();
        bildFilename.append(mask).append("_").append(uniqpart).append(".csv");
        final byte[] content = String.valueOf(stringBuffer).getBytes();
        ftp.putFile(bildFilename.toString(),directoryTo,content);
        stringBuffer.delete(0,stringBuffer.length());
    }
    public StringBuffer createBuffer(){
        final String head = "YEAR;MONTH;DAY;PRODUCT_NAME;STORE_NAME;SALE_TYPE;SOLD_QTY_UNITS;SOLD_QTY_MONEY;STORE_TYPE;STORE_QTY_UNITS;STORE_QTY_MONEY\n";
        stringBuffer.append(head);
        return stringBuffer;
    }
    public StringBuffer addString(final String string){
        stringBuffer.append(string).append("\n");
        return stringBuffer;
    }
    public StringBuffer modifiedString(final String key, final int quantity){
        for (String string : stringBuffer.toString().split("\n")){
            if (string.contains(key)){
                String[] substrings = string.split(";");
                int newQuantity = Integer.parseInt(substrings[9])+quantity;
                StringBuilder sb0 = new StringBuilder();
                sb0.append(";").append(substrings[7]).append(";").append(substrings[8]).append(";").append(substrings[9]).append(";");
                StringBuilder sb1 = new StringBuilder();
                sb1.append(";").append(substrings[7]).append(";").append(substrings[8]).append(";").append(newQuantity).append(";");
                String newSB = stringBuffer.toString().replace(sb0.toString(),sb1.toString());
                stringBuffer.delete(0,stringBuffer.length());
                stringBuffer.append(newSB);
            }
        }
        return stringBuffer;
    }

    public static void main(String[] args) throws Exception {
        new ReportMaker();
    }
}