package script;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    public void log (String text) {
        Date date = new Date();
        SimpleDateFormat timeFormat = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss.SSS");
        System.out.println("[" + timeFormat.format(date) + "] " + text);
    }
}